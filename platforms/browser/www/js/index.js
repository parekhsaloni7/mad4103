var db=null;
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

// event listeners
//========================
document.getElementById("insertHeroes").addEventListener("click", saveButtonPressed);
document.getElementById("showHeroes").addEventListener("click", showButtonPressed);


// my functions go here
//========================
function saveButtonPressed() {
  // debug:
  console.log("save button pressed!");
  console.log("save button pressed!");
alert("save button pressed!");

// 1. get data from USER interface
var n = document.getElementById("name").value;
var d = document.getElementById("available").value;

// DEBUG:
console.log("Name: " + n);
console.log("Available to hire: " + d);

// 2. INSERT INTO DATABASE
db.transaction(
      function(tx){
    //INSERT INTO employees (name, dept) VALUES ("pritesh", "madt");
          tx.executeSql( "INSERT INTO heroes(name, isAvailable) VALUES(?,?)",
          [n, d],
          onSuccessExecuteSql,
          onError )
      },
      onError,
      onReadyTransaction
  )
  alert("save button pressed!");
}

function showButtonPressed() {
  //debug:
  console.log("show button pressed!");
  alert("show button pressed!");
  // 1. RUN YOUR SQL QUERY
db.transaction(
      function(tx){
          tx.executeSql( "SELECT * FROM heroes",
          [],
          displayResults,
          onError )
      },
      onError,
      onReadyTransaction
  )
}
}

function displayResults( tx, results ){

    if(results.rows.length == 0) {
            alert("No records found");
            return false;
        }

        var row = "";
        for(var i=0; i<results.rows.length; i++) {
      document.getElementById("resultsSection").innerHTML +=
          "<p> Name: "
        +   results.rows.item(i).name
        + "<br>"
        + "Available to hire: "
        +   results.rows.item(i).isAvailable
        + "</p>";

        }

    }
//connect to myDatabase


document.addEventListener("deviceready", connectToDatabase)

function connectToDatabase() {
  console.log("device is ready - connecting to database");
  // 2. open the database. The code is depends on your platform!
  if (window.cordova.platformId === 'browser') {
    console.log("browser detected...");
    // For browsers, use this syntax:
    //  (nameOfDb, version number, description, db size)
    // By default, set version to 1.0, and size to 2MB
    db = window.openDatabase("cestar", "1.0", "superdb", 2*1024*1024);
  }
  else {
    alert("mobile device detected");
    console.log("mobile device detected!");
    var databaseDetails = {"name":"superdb", "location":"default"}
    db = window.sqlitePlugin.openDatabase(databaseDetails);
    console.log("done opening db");
  }

  if (!db) {
    alert("databse not opened!");
    return false;
  }

}

function onReadyTransaction( ){
  console.log( 'Transaction completed' )
}
function onSuccessExecuteSql( tx, results ){
  console.log( 'Execute SQL completed' )
}
function onError( err ){
  console.log( err )
}
